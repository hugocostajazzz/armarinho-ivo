<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title></title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main>
            <!--Titulo Seção -->
            <section class="titulo-secao">
                <h1 class="container">Crochê e tricô</h1>
            </section>
            <!--Seção categoria-->
            <section class="categoria-interna">
                <div class="container">
                    <!--Migalha de pão-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Crochê e Tricô</li>
                        </ol>
                    </nav>
                </div>
                <div class="container">
                    <!--Linha-->
                    <div class="linha">
                        <!--Coluna 25%-->
                        <div class="col-small">
                            <!--Lista da categoria-->
                            <ul class="list-unstyled box-categoria">
                                <li><h1 class="chamada-categoria">Categorias</h1></li><!--Titulo da categoria-->
                                <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                                <li><a href="categoriaProduto.php">Agulha Maquina Groz Beckert</a></li>
                                <li><a href="categoriaProduto.php">Agulha Para Crochê</a></li>
                                <li><a href="categoriaProduto.php">Agulha para Crochê Tunisiana</a></li>
                                <li><a href="categoriaProduto.php">Agulha Para Ponto Russo</a></li>
                                <li><a href="categoriaProduto.php">Agulha Para tricô</a></li>
                            </ul>
                            <ul class="list-unstyled box-categoria">
                                <li><h1 class="chamada-categoria">Preço</h1></li><!--Titulo da categoria-->
                                <li><a href="">R$0,00 - R$19,99</a></li>
                                <li><a href="">R$20,00 - R$49,99</a></li>
                                <li><a href="">R$50,00 - R$ 99,99</a></li>
                                <li><a href="">R$100,00 E ACIMA</a></li>
                            </ul>
                            <ul class="list-unstyled box-categoria">
                                <li><h1 class="chamada-categoria">Material</h1></li><!--Titulo da categoria-->
                                <li><a href="">Cetim Engomado</a></li>
                                <li><a href="">Failet / Tafetá (Nobile)</a></li>
                                <li><a href="">Juta</a></li>
                                <li><a href="">Tela Escócia (Decortela)</a></li>
                            </ul>
                        </div>
                        <!--Coluna 75%-->
                        <div class="col-big">
                            <header><!--Header-->
                                <div class="input-group selectgroup"><!-- Select odernar-->
                                    <label for="selectOrdernar">Ordenar por: </label>
                                    <select class="custom-select" id="selectOrdernar">
                                        <option selected>Novidades</option>
                                        <option value="1">Mais Vendidos</option>
                                        <option value="2">Em Promoção</option>
                                    </select>
                                </div>
                                <div class="group-pagination"><!-- Select exibir-->
                                    <label for="selectExibir">Exibir:</label>
                                    <select class="custom-select" id="selectExibir">
                                        <option selected>12</option>
                                        <option value="1">16</option>
                                        <option value="2">20</option>
                                        <option value="2">24</option>
                                    </select>
                                    <nav>
                                        <ul class="pagination pagination-sm "><!--Paginação-->
                                            <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </header>
                            <ul class="list-unstyled apresentacao-produto">
                                <li>
                                    <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo Circulado da circula</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 129,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 29,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbsssssssssante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                                <li>
                                   <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                                    <span class="nome-produto">Barbante Apolo</span>
                                    <span class="marca-produto">Círculo</span>
                                    <span class="promocao">de R$15,90</span>
                                    <span class="preco">R$ 9,90</span>
                                    <button onclick="window.location.href='carrinho.php'">Comprar</button>
                                </li>
                            </ul>
                            <div class="filtro-footer">
                                <div class="group-pagination"><!-- Select exibir-->
                                    <label for="selectExibir">Exibir:</label>
                                    <select class="custom-select" id="selectExibir">
                                        <option selected>12</option>
                                        <option value="1">16</option>
                                        <option value="2">20</option>
                                        <option value="2">24</option>
                                    </select>
                                    <nav>
                                        <ul class="pagination pagination-sm "><!--Paginação-->
                                            <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require 'templates/contentBottom.php'?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

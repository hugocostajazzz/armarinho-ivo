<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title></title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main>
            <section class="titulo-secao">
                <h1 class="container">Crochê e Tricô</h1>
            </section>
            <section class="produto-interno">
                <div class="container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a href="categoriaProduto.php">Crochê e Tricô</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Linha para crochê Coats Mirela c/ 1000 m</li>
                        </ol>
                    </nav>
                </div>   
                <div class="cont-destaque container small">
                    <div class="img">
                        <div class="img-destaque">
                        <img src="assets/imgs/produtoDestaque.png" alt="">
                        </div>
                        <aside>
                            <img src="assets/icons/estrelas.jpg" alt="">
                            <div class="seguranca">
                            <img src="assets/icons/seguranca.jpg" alt=""><span>Loja 100% Segura</span>
                            </div>
                        </aside>
                    </div>
                    <div class="texto-destaque">
                        <h1>Linha para crochê Coats Mirela c/ 1000 m</h1>
                        <span class="codigo-produto">cod do produto: 000-000</span>
                        <span class="caract-produto">Característica do Produto</span>
                        <ul class="list-unstyled caract-lista">
                            <li>Marca: Coats Corrente</li>
                            <li>Referência: Mirella</li>
                            <li>Etiqueta: 100</li>
                            <li>Tex 145</li>
                            <li>Cor: Creme</li>
                            <li>Composição: 100% algodão</li>
                            <li>Unidade de venda: Novelo c/ 1000 mts</li>
                        </ul>
                        <div class="preco-destaque">
                            <h1>R$ 9,90</h1>
                            <span>ou em 10x R$ 0,99</span>
                        </div>
                        <div class="input-group">
                            <label for="#number">Qtd:</label>
                            <input id="number" type="number"value="1">
                            <button class="comprar" onclick="window.location.href='carrinho.php'">Comprar</button>
                        </div>
                        <div class="frete">
                            <span>Calcular Frete | CEP</span>
                            <div class="input-group">
                                <input type="text" aria-label="cep">
                                <button>Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
                <aside class="container small cores-disponiveis">
                    <header>
                        <h1>Cores Disponíveis</h1>
                    </header>
                    <ul class="list-unstyled lista-cores">
                        <li>
                            <div class="img-cores">
                                <img src="assets/imgs/produto-carrinho.png" alt="">
                            </div>
                            <div class="txt-cores">
                                <span>Rosa</span>
                                <span>cod. 0000-0000</span>
                            </div>
                            <div class="input-group">
                                <label for="#number">Qtd:</label>
                                <input id="number" type="number"value="0">
                            </div>
                        </li>
                        <li>
                            <div class="img-cores">
                                <img src="assets/imgs/produto-carrinho.png" alt="">
                            </div>
                            <div class="txt-cores">
                                <span>Rosa</span>
                                <span>cod. 0000-0000</span>
                            </div>
                            <div class="input-group">
                                <label for="#number">Qtd:</label>
                                <input id="number" type="number"value="0">
                            </div>
                        </li>
                        <li>
                            <div class="img-cores">
                                <img src="assets/imgs/produto-carrinho.png" alt="">
                            </div>
                            <div class="txt-cores">
                                <span>Rosa</span>
                                <span>cod. 0000-0000</span>
                            </div>
                            <div class="input-group">
                                <label for="#number">Qtd:</label>
                                <input id="number" type="number"value="0">
                            </div>
                        </li>
                        <li>
                            <div class="img-cores">
                                <img src="assets/imgs/produto-carrinho.png" alt="">
                            </div>
                            <div class="txt-cores">
                                <span>Rosa</span>
                                <span>cod. 0000-0000</span>
                            </div>
                            <div class="input-group">
                                <label for="#number">Qtd:</label>
                                <input id="number" type="number"value="0">
                            </div>
                        </li>
                        <li>
                            <div class="img-cores">
                                <img src="assets/imgs/produto-carrinho.png" alt="">
                            </div>
                            <div class="txt-cores">
                                <span>Rosa</span>
                                <span>cod. 0000-0000</span>
                            </div>
                            <div class="input-group">
                                <label for="#number">Qtd:</label>
                                <input id="number" type="number"value="0">
                            </div>
                        </li>
                        <li>
                            <div class="img-cores">
                                <img src="assets/imgs/produto-carrinho.png" alt="">
                            </div>
                            <div class="txt-cores">
                                <span>Rosa</span>
                                <span>cod. 0000-0000</span>
                            </div>
                            <div class="input-group">
                                <label for="#number">Qtd:</label>
                                <input id="number" type="number"value="0">
                            </div>
                        </li>
                    </ul>
                    <div class="add-btn">
                        <button><i class="fas fa-shopping-cart"></i> <span>Adicionar produtos ao carrinho</span></button>
                    </div>
                </aside>
                <div class="info-produto container small">
                    <div class="txt-info">
                        <h1>Detalhes do produto</h1>
                        <p>O Mirella possui um excelente rendimento com 1000 metros de fio por novelo. É um fio 100% algodão, que recebe um tratamento especial para impedir a pilosidade e garantir uma maciez única aos trabalhos manuais! Especialmente perfeitos para peças de vestuário, pois possui PH neutro - e assim, não causa irritação ao entrar em contato com a pele</p>
                    </div>
                    <div class="opiniao">
                        <h1>Opinião dos clientes</h1>
                        <button>Seja o primeiro a avaliar este produto</button>
                    </div>
                </div>
                <div class="container small relacionados-prod">
                    <h1>Produtos Relacionados</h1>
                    <ul class="list-unstyled apresentacao-produto">
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo Circulado da circula</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>      
                    </ul>
                </div>
                <?php require 'templates/contentBottom.php'?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

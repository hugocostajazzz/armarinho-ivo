<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Trabalhe Conosco</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main>
            <section class="titulo-secao">
                <h1 class="container">Trabalhe Conosco</h1>
            </section>
            <section class="trabalhe">
                <div class="container small cont-trabalhe">
                    <div class="texto-trabalhe">
                        <p>Para trabalhar conosco, preencha os dados<br> ao lado e em breve retornaremos seu contato.
                        </p>
                        <p>Se preferir, entre em contato diretamente através de:</p>
                        <a class="email-trabalhe" href="">contato@armarinhoivo.com.br</a>
                    </div>
                    <form action="">
                    <div class="form-group">    
                            <input type="text" required class="form-control" id="input_nome"  placeholder="Digite seu nome*">
                        </div>
                        <div class="form-group">    
                            <input type="email"  required class="form-control" id="input_email" placeholder="Digite seu email*">
                        </div>
                        <div class="form-group">    
                            <input type="tel" required class="form-control" id="input_tel" placeholder="Digite seu telefone*">
                        </div>
                        <div class="form-group">    
                            <input type="text" required class="form-control" id="input_area" placeholder="Digite a sua área de atuação*">
                        </div>
                        <div class="curriculo">
                            <span>Currículo * <strong>Arquivos tipo: .doc, .docx ou .pdf</strong></span>
                            <div class="curriculo-group">
                                <input type="file" id="file-input" required  class="input-file">
                                <label for="file-input" class="label-file">Escolher Arquivo</label>
                            </div>
                        </div>
                        <div class="group-form">
                            <button class="btn-form">Enviar</button>
                        </div>
                    </form>
                </div>
                <?php require 'templates/contentBottom.php'?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

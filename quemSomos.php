<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Quem Somos</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main class="bg">
            <section class="titulo-secao">
                <h1 class="container">Quem Somos</h1>
            </section>
            <section class="quemSomos">
                <div class="container small">
                    <h1 class="titulo-interno">Armarinho Ivo</h1>
                    <img class="img-fluid banner-quemSomos" src="assets/imgs/temporario.png" alt="Banner da seção quem somos">
                    <p><strong>Mollit eiusmod incididunt reprehenderit est laboris tempor in nulla. Cupidatat aliqua aute reprehenderit consequat consequat irure ipsum irure sit. Consectetur qui tempor et laborum laborum non est duis non nostrud ipsum velit tempor laborum. Ut nisi sunt consectetur velit pariatur ad duis labore id Lorem. Excepteur minim consequat sunt incididunt ullamco ut. Et veniam amet nostrud in adipisicing in.</strong></p>
                    <p>Culpa laborum voluptate adipisicing commodo nisi elit laborum in anim cupidatat duis eiusmod excepteur laborum. Cillum dolor aute esse magna laboris ut do. Ad aliqua officia elit dolor dolor incididunt tempor. Aliquip esse minim minim proident magna veniam reprehenderit incididunt. Ut duis ex et adipisicing.</p>
                    <p>Dolor ipsum qui adipisicing in ea excepteur elit in elit ullamco sit. Adipisicing magna ipsum ullamco labore officia voluptate eu occaecat. Duis magna do sint minim do tempor consectetur consequat cupidatat in. Reprehenderit cupidatat velit est ipsum fugiat minim sint et incididunt deserunt. Ut exercitation nisi dolor cillum.</p>
                    <p>Ullamco tempor cillum ex exercitation velit enim laboris et. In cupidatat incididunt incididunt sint ex. Dolore mollit esse eiusmod exercitation occaecat. Laborum voluptate in tempor enim reprehenderit aliqua exercitation in quis pariatur ullamco. Ea enim fugiat nulla cillum veniam ex veniam aliquip consectetur. Ipsum exercitation sint reprehenderit magna mollit ea ex consequat id irure incididunt. Duis culpa ea labore velit nisi proident.</p>
                    <div class="video">
                        <iframe width="80%" height="500" src="https://www.youtube.com/embed/0NltAw6ZH4U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <?php require 'templates/contentBottom.php'?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

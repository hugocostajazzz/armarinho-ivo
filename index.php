<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Home</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
    <?php require 'templates/header.php' ?>
    <main class="bg">
        <section class="banner container">
            <div class="bd-example">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="assets/imgs/banner.jpg" class="d-block w-100" alt="assets/imgs/banner.jpg">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/imgs/banner.jpg" class="d-block w-100" alt="assets/imgs/banner.jpg">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <?php require 'templates/contentBottom.php' ?>
        </section>
        <section class="cat-destaque">
            <h1 class="text-center">Categorias em destaque</h1>
            <div class="container">
               <ul class="list-unstyled">
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-zipe.png" alt=""><span>Armarinhoo, Botões e Zíper</span></a></li>
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-barbantes.png" alt=""><span>Barbantes</span></a></li>
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-croche.png" alt=""><span>Crochê e Tricô</span></a></li>
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-fitas.png" alt=""><span>Fitas e Laços</span></a></li> 
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-fios.png" alt=""><span>Lãs e Fios</span></a></li> 
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-papelaria.png" alt=""><span>Papelaria e Embalagens</span></a></li> 
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-patch.png" alt=""><span>Patchwork e Tecidos</span></a></li> 
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-scrap.png" alt=""><span>Scrapbook</span></a></li> 
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-decoupage.png" alt=""><span>Decoupage</span></a></li> 
                    <li><a href="categoriaProduto.php"><img src="assets/imgs/cat-agulhas.png" alt=""><span>Agulhas</span></a></li>  
               </ul>
            </div>
        </section>
        <section class="produto-destaque container">
            <div class="linha">
                <div class="col-small">
                    <span class="txt-destaque">Produtos em detaque >></span>
                </div>
                <div class="col-big">
                    <header>
                        <div class="cont-btn">
                            <button class="btn-destaque btn-ativo">Destaques</button>
                            <button class="btn-destaque ">Mais vendidos</button>
                            <button class="btn-destaque ">Promoções</button>
                            <button class="btn-destaque ">Novos</button>
                        </div>
                    </header>
                </div>
            </div>
            <div class="linha">
                <div class="col-small">
                    <nav>
                        <ul class="list-unstyled">
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                            <li><a href="categoriaProduto.php">Agulha de Mão e para Bordar</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-big">
                    <ul class="list-unstyled apresentacao-produto">
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                       <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                       <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                       <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                        <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                       <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                       <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                       <li>
                            <a href="produtoDestaque.php"><img src="assets/imgs/produto.png" alt=""></a>
                            <span class="nome-produto">Barbante Apolo</span>
                            <span class="marca-produto">Círculo</span>
                            <span class="promocao">de R$15,90</span>
                            <span class="preco">R$ 9,90</span>
                            <button onclick="window.location.href='carrinho.php'">Comprar</button>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="banner-produto container">
            <ul class="list-unstyled">
                <li><a href="produtoDestaque.php"><img class="img-fluid" src="assets/imgs/bannerProduto.jpg" alt=""></a></li>
                <li><a href="produtoDestaque.php"><img class="img-fluid" src="assets/imgs/bannerProduto.jpg" alt=""></a></li>
                <li><a href="produtoDestaque.php"><img class="img-fluid" src="assets/imgs/bannerProduto.jpg" alt=""></a></li>
            </ul>
        </section>
    </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Como Funciona</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main>
            <section class="titulo-secao">
                <h1 class="container">Como Funciona</h1>
            </section>
            <section class="faq">
                <div class="container small faq-content">
                    <div class="faq-conteudo">
                        <h1>Como Funciona Lorem</h1>
                        <p>Aute pariatur eiusmod Lorem nostrud do proident magna amet non aliqua in sit dolore pariatur. Consectetur quis velit ut officia nisi nulla minim ipsum proident. Esse minim sint exercitation anim eiusmod proident deserunt occaecat cillum aliqua consequat laborum commodo minim.</p>
                    </div>
                    <div class="faq-conteudo">
                        <h1>Como Funciona Lorem</h1>
                        <p>Aute pariatur eiusmod Lorem nostrud do proident magna amet non aliqua in sit dolore pariatur. Consectetur quis velit ut officia nisi nulla minim ipsum proident. Esse minim sint exercitation anim eiusmod proident deserunt occaecat cillum aliqua consequat laborum commodo minim.</p>
                    </div>
                    <div class="faq-conteudo">
                        <h1>Como Funciona Lorem</h1>
                        <p>Aute pariatur eiusmod Lorem nostrud do proident magna amet non aliqua in sit dolore pariatur. Consectetur quis velit ut officia nisi nulla minim ipsum proident. Esse minim sint exercitation anim eiusmod proident deserunt occaecat cillum aliqua consequat laborum commodo minim.</p>
                    </div>
                    <div class="faq-conteudo">
                        <h1>Como Funciona Lorem</h1>
                        <p>Aute pariatur eiusmod Lorem nostrud do proident magna amet non aliqua in sit dolore pariatur. Consectetur quis velit ut officia nisi nulla minim ipsum proident. Esse minim sint exercitation anim eiusmod proident deserunt occaecat cillum aliqua consequat laborum commodo minim.</p>
                    </div>
                </div>
                <?php require 'templates/contentBottom.php' ?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

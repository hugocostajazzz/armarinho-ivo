<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Carrinho de Compras</title>
</head>
<body style="overflow-x:hidden">
    <!-- HEADER -->
    <?php require 'templates/header.php' ?>
        <main>
            <section class="titulo-secao">
                <h1 class="container">Carrinho de compras</h1>
            </section>
            <section class="carrinho">
                <div class="produtos container small">
                    <div class="titulos">
                        <div class="produto-titulo">
                            <h1>Produto</h1>
                        </div>
                        <div class="preco-titulo">
                            <h1>Preço</h1>
                            <h1>Quant.</h1>
                            <h1>Sub-total</h1>
                        </div>
                    </div>
                    <ul class="list-unstyled lista-carrinho">
                        <li>
                            <div class="cont-lista">
                                <div class="img-produto">
                                    <img src="assets/imgs/produto-carrinho.png" alt="">
                                </div>
                                <div class="prod-carrinho">
                                    <h1>Linha para crochê Coats Mirela c/ 1000m</h1>
                                    <span class="cor">Cor: Creme</span>
                                    <span class="cod">Cód. do Produto: 0000-0000</span>
                                </div>
                            </div>
                            <div class="cont-preco">
                                <div class="preco">
                                    <span>R$129,90</span>
                                    <input id="number" type="number" value="1">
                                    <span>R$ 9,90</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="sub-total">
                        <h1>Sub-total:</h1><h1>R$29,70</h1>
                    </div>
                    <div class="desconto-cont">
                        <div class="desconto">
                            <h1>Cupom de Desconto</h1>
                            <span>Digite abaixo seu cupom de desconto.</span>
                            <div class="input-group">
                                <input type="text">
                                <button>Aplicar</button>
                            </div>
                        </div>
                        <div class="frete">
                            <h1>Calcular Frete</h1>
                            <span>Informe seu CEP para calcular o valor da entrega.</span>
                            <div class="input-group">
                                <input type="number">
                                <button>Calcular</button>
                            </div>
                        </div>
                    </div>
                        <div class="total">
                            <h1>Total:</h1>
                            <h1>R$ 28,00</h1>
                        </div>
                        <div class="finalizar">
                            <div class="cartoes">
                                <ul class="list-unstyled">
                                    <li><img src="assets/icons/visa-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/master-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/hiper-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/american-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/club-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/elo-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/aura-carrinho.jpg" alt=""></li>
                                    <li><img src="assets/icons/boleto-carrinho.jpg" alt=""></li>
                                </ul>
                                <button>Finalizar Pedido</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require 'templates/contentBottom.php' ?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

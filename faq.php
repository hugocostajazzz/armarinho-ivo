<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Perguntas Frequentes</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main>
            <section class="titulo-secao">
                <h1 class="container">Perguntas Frequentes</h1>
            </section>
            <section class="faq">
                <div class="container small">
                    <h1>Abaixo tiramos as dúvidas mais frequentes dos nossos clientes. Confira.</h1>
                </div>
                <div class="container small faq-content">
                    <div class="faq-conteudo">
                        <h1>Procedimento de compra. Como realizar?</h1>
                        <ul class="list-unstyled">
                            <li>1. Adicione quantos produtos quiser ao carrinho</li>
                            <li>2. Após estar com todos os produtos que deseja no carrinho, confira as quantidades e clique em CONCLUIR</li>
                            <li>3. Se for cadastrado, digite seu login e senha. Se não for, informe o CEP e clique em AVANÇAR para fazer o cadastro antes de concluir a compra.</li>
                            <li>4. Escolha o endereço de entrega e uma das modalidades de frete disponíveis. Clique em CONTINUAR</li>
                            <li>5. O próximo passo é escolher a forma de pagamento. Escolha e clique em AVANÇAR.</li>
                            <li>6. Escolha a forma de pagamento. Você será levado ao ambiente de pagamento para pagamentos em boleto/transferência bancária ou Cielo para pagamento com cartão de crédito. Dependendo do cartão, ainda será levado ao ambiente do banco para confirmar a autenticidade do usuário.</li>
                            <li>7. Pedido finalizado. O sitema vai gerar um número para que você possa acompanhar o status de seu pedido pelo site, através</li>
                            <li>Se a dúvida persistir, entre em contato conosco:<br><a href="">Chat</a> ou pelo Fale Conosco ou pelo telefone <a href="">81 3000-0000</a></li>
                        </ul>
                    </div>
                    <div class="faq-conteudo">
                        <h1>Como sei que concluí meu pedido?</h1>
                        <p>Seu pedido está concluído quando o sistema gerar um NÚMERO para acompanhamento. Você pode monitorar a movimentação - desde a aprovação até o despacho  - no próprio site, clicando em MEUS PEDIDOS, no canto superior direito da página, ou pela Central do Cliente. Você também receberá um e-mail automático, informando a cada passo o status do seu pedido.</p>
                    </div>
                    <div class="faq-conteudo">
                        <h1>Como funciona o pagamento?</h1>
                        <p>Você pode pagar suas compras com cartão de crédito, boleto bancário (à vista, com 5% de desconto) ou por transferência bancária (apenas para clientes do Banco Itaú). Saiba mais clicando em FORMAS DE PAGAMENTO, no rodapé do site</p>
                    </div>
                    <div class="faq-conteudo">
                        <h1>Como funciona a entrega?</h1>
                        <p>O <strong>Armarinho Ivo</strong> trabalha com frota própria para entregas na cidade do Recife, sem cobrança de taxa para compras acima de R$ 250,00. Para os municípios da RMR, interior e outros estados, clique em FORMAS DE ENTREGA, no rodapé do site. Em breve estará disponível também o frete via transportadora.</p>
                    </div>
                    <!-- <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Collapsible Group Item #1
                                 </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Collapsible Group Item #2
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div> -->
                </div>
                <?php require 'templates/contentBottom.php' ?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

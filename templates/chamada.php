<!-- Meta tag's-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"><!--reconhece tamanho da tela de diversos dispostivos-->
<meta name="keywords" content="">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="author" content="Jazzz Agência Digital | Hugo Costa">
<meta property="og:site_name" content=""/>
<!--CSS-->
<link rel="shortcut icon" type="image/x-icon" href="assets/icons/favicon.png"/>
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/normalize.css">
<link rel="stylesheet" href="assets/css/main.css">

<!--Font awesome-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- JS -->
<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/js/bootstrap.bundle.js"></script>
<script src="assets/js/main.js"></script>
<footer class="container">
  <div class="topo">
    <div class="assine">
      <img src="assets/icons/icone_evelope.png" alt="Assine nossas Promoções">
      <ul class="list-unstyled">
        <li>Assine nossas promoções</li>
        <li>Receba ofertas e descontos exclusivos em seu e-mail</li>
      </ul>
    </div>
    <div class="input-group filtro-footer">
        <input type="text" class="form-control" placeholder="Cadastre seu email" aria-label="Cadastre seu email" aria-describedby="button-addon2">
        <div class="input-group-append">
            <button class="btn btn-filtro" type="button" id="button-addon2">Cadastrar</button>
        </div>
    </div>
  </div>
  <div class="central">
    <div class="cont-central">
        <address>
            <a href="./"><img src="assets/icons/logo_rodape.png" alt="Logo Armarinho Ivo"></a>
            <ul class="list-unstyled">
              <li>CNPJ: 00.000.000/0001-00</li>
              <li>Rua de Santa Rita, 171</li>
              <li>São José, Recife / PE</li>
              <li>Fone: 81 <strong>3424 1182</strong></li>
              <li><a href="mailto:ac@armarinhoivo.com.br">sac@armarinhoivo.com.br</a></li>
            </ul>
        </address>
        <div class="facebook">
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Farmarinhoivo&tabs&width=340&height=130&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="340" height="181" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
        </div>
        <ul class="list-unstyled mapaSite">
            <li>Institucional</li>
            <li><a href="quemSomos.php">Sobre a empresa</a></li>
            <li><a href="trabalheConosco.php">Trabalhe conosco</a></li>
            <li><a href="faleConosco.php">Fale conosco</a></li>
            <li><a href="https://www.jazzz.com.br/homologacao/blog/">Blog</a></li>
        </ul>
        <ul class="list-unstyled mapaSite">
          <li>Políticas</li>
          <li><a href="politicas.php#politica">Política de privacidade</a></li>
          <li><a href="politicas.php#qualidade">Política de qualidade</a></li>
          <li><a href="politicas.php#entrega">Política de entrega</a></li>
          <li><a href="politicas.php#troca">Sobre troca e devolução</a></li>
        </ul>
        <ul class="list-unstyled mapaSite">
          <li>Suporte</li>
          <li><a href="comoFunciona.php">Como funciona</a></li>
          <li><a href="faq.php">Perguntas frequentes</a></li>
        </ul>
      </div>
      <ul class="list-unstyled social-rodape">
        <li><a href="https://www.facebook.com/armarinhoivo" target="_blank"><img src="assets/icons/facebook.jpg" alt=""></a></li>
        <li><a href="https://www.instagram.com/armarinhoivo/" target="_blank"><img src="assets/icons/instagram.jpg"  alt=""></a></li>
        <!-- <li><a href=""><img src="assets/icons/linkedin.jpg"  alt=""></a></li> -->
      </ul>
  </div>
  <div class="bottom">
      <div class="seguranca">
          <span>Segurança</span>
          <ul class="list-unstyled">
            <li><img src="assets/icons/ssl.png" alt="Certificado de Segurança SSL"></li>
            <li><img src="assets/icons/googe.png" alt="Navegação segura Google"></li>
            <li><img src="assets/icons/ebit.png" alt="EBit"></li>
          </ul>
      </div>
      <div class="formapagamento">
        <span>Formas de Pagamento</span>
        <ul class="list-unstyled">
          <li><img src="assets/icons/visa.png" alt="Cartão Visa"></li>
          <li><img src="assets/icons/master.png" alt="Cartão Master"></li>
          <li><img src="assets/icons/elo.png" alt="Elo"></li>
          <li><img src="assets/icons/america.png" alt="American Express"></li>
          <li><img src="assets/icons/descover.png" alt="Descover"></li>
          <li><img src="assets/icons/club.png" alt="Club"></li>
          <li><img src="assets/icons/aura.png" alt="Aura"></li>
          <li><img src="assets/icons/jcb.png" alt="JCB"></li>
          <li><img src="assets/icons/boleto.png" alt="Boleto bancário"></li>
        </ul>
      </div>
      <div class="jazzz">
        <span>Desenvolvido</span>
          <a href="http://jazzz.com.br" target="_blank"> <img src="assets/icons/jazzz.jpg" title="Desenvolvido pela Jazzz Agência Digital." alt=""></a>
      </div>
  </div>
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5db0a8f278ab74187a5b3688/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
    })();
  </script>
<!--End of Tawk.to Script-->
</footer>

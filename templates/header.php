<header class="cabecalho">
  <div class="topo">
      <div class="cont-topo container">
        <div class="televendas">
          <span>TELEVENDAS: (81) 3424.1182</span><span> / </span> <span>Seg à Sex das 8H00 às 17H45</span> 
        </div>
        <div class="local-topo">
          <a target="_blank" href="https://www.google.com/maps?q=armarinho+ivo&biw=1920&bih=969&um=1&ie=UTF-8&sa=X&ved=0ahUKEwjX_OfrmrPlAhVYHLkGHUvOAJoQ_AUIEygC">Rua de Santa Rita, 171 - São José - Recife - </a><a href="mailto:sac@armarinhoivo.com.br">sac@armarinhoivo.com.br</a>
        </div>
      </div>
  </div>
  <div class="central">
    <div class="cont-central container">
        <div class="logo">
          <a href="./"><img src="assets/imgs/logo-armarinho.png" alt="Logo Armarinho Ivo"></a>
        </div>
          <div class="input-group filtro">
            <input type="text" class="form-control" placeholder="Encontre o produto" aria-label="Encontre o produto" aria-describedby="button-addon2">
            <div class="input-group-append">
              <button class="btn btn-filtro" type="button" id="button-addon2"><i class="fas fa-search"></i><span>Buscar</span></button>
            </div>
          </div>
        <ul class="list-unstyled">
            <li class="icone-hamburger"><img src="assets/icons/menu.png" alt="" onclick="myFunction()"></li>
            <li><a href=""><img src="assets/icons/user.png" alt="Ícone de Usuário"></a></li>
            <li><a href="carrinho.php"><img src="assets/icons/carrinho.png" alt="Ícone de Carrinho de compra"></a></li>
        </ul>
    </div>
  </div>
  <div class="inferior">
      <div class="menu-produto"  >
        <ul class="list-unstyled" id="myLinks">
          <!-- <li><a href=""><img src="assets/icons/menu.png" alt=""></a></li> -->
          <li><a href="categoriaProduto.php">Agulhas</a></li>
          <li><a href="categoriaProduto.php">Botões e Zíper</a></li>
          <li><a href="categoriaProduto.php">Barbantes</a></li>
          <li><a href="categoriaProduto.php">Bordardes e Viés</a></li>
          <li><a href="categoriaProduto.php">Crochê e Tricô</a></li>
          <li><a href="categoriaProduto.php">Fitas e Laços</a></li>
          <li><a href="categoriaProduto.php">Lãs e Fios</a></li>
          <li><a href="categoriaProduto.php">Linhas Bordar</a></li>
          <li><a href="categoriaProduto.php">Linhas Costura</a></li>
          <li><a href="categoriaProduto.php">Meia de Seda e Biscuit</a></li>
          <li><a href="categoriaProduto.php">Papelaria e embalagens</a></li>
          <li><a href="categoriaProduto.php">Scrap e Dacoupage</a></li>
          <li><a href="categoriaProduto.php">Marcas</a></li>
        </ul>
      </div>
  </div>
</header>

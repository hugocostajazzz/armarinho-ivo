<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Políticas</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <main>
        <section class="titulo-secao">
            <h1 class="container">Políticas</h1>
        </section>
        <section class="politicas">
            <div class="container small">
                <h1>Informe-se a respeito das nossas políticas de funcionamento e atendimento</h1>
            </div>
            <div class="container small cont-politicas">
                <nav>
                    <ul class="list-unstyled">
                        <li><a href="#politica">Privacidade</a></li>
                        <li><a href="#qualidade">Qualidade</a></li>
                        <li><a href="#entrega">Entrega</a></li>
                        <li><a href="#seguranca">Segurança</a></li>
                        <li><a href="#troca">Troca e Devoluções</a></li>
                    </ul>
                </nav>
                <div class="texto">
                    <div id="politica" class="politica">
                        <h1>Política de Privacidade</h1>
                        <p>Todas as informações e transações que trafegam pela loja virtual Armarinho Ivo são protegidas por protocolo de segurança padrão SSL com criptografia forte de 128 bits, certificado também pela Thawte, uma das maisores empresas de certificação de segurança no mundo digital, especializada em segurança no mercado virtual mundial.</p>
                    </div>
                    <div id="qualidade" class="politica">
                        <h1>Política de Qualidade</h1>
                        <p>O prazo de entrega dos produtos varia de acordo com o local solicitado para entrega, forma de pagamento escolhida e disponibilidade do produto adquirido, sendo dado sempre ao cliente uma estimativa média de entrega podendo sofrer variações por motivos superiores.</p>
                        <p>ATENÇÃO: O prazo para entrega do pedido passa a ser considerado a partir da aprovação do pagamento por parte da instituição financeira e após a validação dos dados cadastrais.</p>
                    </div>
                    <div id="entrega" class="politica">
                        <h1>Política de Entrega</h1>
                        <p>O prazo de entrega dos produtos variam de acordo com o local solicitado para entrega, forma de pagamento escolhida e disponibilidade do produto adquirido, sendo dado sempre ao cliente uma estimativa média de entrega podendo sofrer variações por motivos superiores.</p>
                        <p>ATENÇÃO: O prazo para entrega do pedido passa a ser considerado a partir da aprovação do pagamento por parte da instituição financeira e após a validação dos dados cadastrais.</p>
                    </div>
                    <div id="seguranca" class="politica">
                        <h1>Política de Segurança</h1>
                        <p>Toda a nossa estrutura está protegida por sevidores e sistemas de segurança (firewalls), hospedada em Datacenter, seguindo rigorosas normas internacionais de segurança física e lógica. Todas as informações e transações que trafegam pela loja virtual Armarinho Ivo são protegidas por protocolo de segurança padrão SSL, com criptografia forte de 128 bits, certificado também pela Thawte, uma das maiores empresas de certificação de segurança no mundo digital, espcializada em segurança no mercado virtual mundial.</p>
                    </div>
                    <div id="troca" class="politica">
                        <h1>Política de Troca e devoluções</h1>
                        <p>O Armarinho Ivo preocupa com a total satisfação, oferece produtos diferenciados, com excelência e qualidade, buscando sempre atender as necessidades do mercado para trazer o que há de melhor para seus clientes.</p>
                        <p>Em virtude disso, para manter a credibilidade que conquistou e tanto preza, vislumbrando alcançar sempre o maior nível de satisfação dos consumidores, o Armarinho Ivo criou uma Pilítica de Troca e Devolução, a fim de levar amplamente ao conhecimento de seu cliente, o procedimento adotado por esta empresa em decorrência da ocasião.</p>
                        <strong>Regras Gerais:</strong>
                        <p>Caso ocorra necessidade detroca ou devolução, nos casos em que a mesma seja viável, faz-se necessário em primeiro plano, que o cliente entre em contato através do nosso link CONTATO;</p>
                        <p>É imprescindível que se observe as regras que ensejam a devolução, devendo o cliente recusar seu recebimento no momento da entrega, sob pena de não fazer jus à troca.</p>
                    </div>
                </div>
            </div>
            <?php require 'templates/contentBottom.php' ?>
        </section>
    </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>

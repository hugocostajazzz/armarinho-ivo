<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Armarinho Ivo - Fale Conosco</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
        <main>
            <section class="titulo-secao">
                <h1 class="container">Fale Conosco</h1>
            </section>
            <section class="contato">
                <div class="container small cont-contato">
                    <div class="texto-contato">
                        <p>Para maiores Informações, preencha os dados<br> ao lado e em breve retornaremos seu contato.
                        </p>
                        <p>Se preferir, entre em contato através dos dados abaixo:</p>
                        <a class="email-contato" href="">contato@armarinhoivo.com.br</a>
                        <address>
                            <ul class="list-unstyled">
                                <li>Endereço Loja 01</li>
                                <li>Rua de Santa Rita, 171, São José, Recife-PE</li>
                                <li>CEP 52050-000</li>
                                <li>Telefone: 81 3424 1182</li>
                            </ul>
                        </address>
                        <address>
                            <ul class="list-unstyled">
                                <li>Endereço Loja 01</li>
                                <li>Rua de Santa Rita, 171, São José, Recife-PE</li>
                                <li>CEP 52050-000</li>
                                <li>Telefone: 81 3424 1182</li>
                            </ul>
                        </address>
                        <a class="btn-padrao" href="">Ver mapa</a>
                    </div>
                    <form action="">
                        <div class="form-group">    
                            <input type="name" required class="form-control" id="input_nome"  placeholder="Digite seu nome *">
                        </div>
                        <div class="form-group">    
                            <input type="email" required class="form-control" id="input_email" placeholder="Digite seu email *">
                        </div>
                        <div class="form-group">    
                            <input type="tel" required class="form-control" id="input_tel" placeholder="Digite seu telefone *">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="textearea" rows="5" placeholder="Escreva sua mensagem"></textarea>
                        </div>
                        <div class="group-form">
                            <button class="btn-padrao">Enviar</button>
                        </div>
                    </form>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.305300307002!2d-34.87824856214274!3d-8.070310668863362!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab18b14574d00f%3A0x2cd0180b473adaad!2sR.%20Santa%20Rita%2C%20171%20-%20S%C3%A3o%20Jos%C3%A9%2C%20Recife%20-%20PE%2C%2050020-320!5e0!3m2!1spt-BR!2sbr!4v1568981529924!5m2!1spt-BR!2sbr" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                <?php require 'templates/contentBottom.php'?>
            </section>
        </main>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="js/main.js"></script>
</body>
</html>
